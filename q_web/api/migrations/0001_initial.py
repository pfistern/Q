# Generated by Django 3.2.6 on 2021-09-13 21:38

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='ACLModel',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='', max_length=255)),
                ('allow', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='APIToken',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('expire', models.DateTimeField()),
                ('token', models.CharField(default='', max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='ACLGroupModel',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='', max_length=255, unique=True)),
                ('linked_acls', models.ManyToManyField(to='api.ACLModel')),
            ],
        ),
        migrations.CreateModel(
            name='AccountModel',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('internal_user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('linked_acl_group', models.ForeignKey(default=2, on_delete=django.db.models.deletion.DO_NOTHING, to='api.aclgroupmodel')),
                ('linked_api_tokens', models.ManyToManyField(blank=True, to='api.APIToken')),
            ],
        ),
    ]
